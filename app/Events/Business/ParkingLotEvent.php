<?php namespace App\Events\Business;

use App\Events\Event;
use App\Models\Business\ParkingLot\ParkingLot;
use App\Models\Settings\ParkingSpace\ParkingSpace;
use Illuminate\Queue\SerializesModels;

class ParkingLotEvent extends Event
{
    use SerializesModels;

    /**
     * @var ParkingSpace
     */
    public $parking_space;

    /**
     * @var string
     */
    public $action;

    /**
     * @var ParkingLot
     */
    public $parking_lot;

    /**
     * Create a new event instance.
     *
     * @param ParkingSpace $parking_space
     * @param ParkingLot $parking_lot
     * @param string $action
     */
    public function __construct( ParkingSpace $parking_space, ParkingLot $parking_lot, $action )
    {
        $this->parking_space    = $parking_space;
        $this->action           = $action;
        $this->parking_lot      = $parking_lot;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

}

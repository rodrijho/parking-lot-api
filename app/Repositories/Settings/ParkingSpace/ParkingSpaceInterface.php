<?php namespace App\Repositories\Settings\ParkingSpace;

use Illuminate\Http\Request;

interface ParkingSpaceInterface
{
    /**
     * Return the list of the available parking spaces
     *
     * @param Request $request
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getAll( Request $request, $order_by = 'name', $sort = 'asc' );

    /**
     * Store a new parking space in database
     *
     * @param Request $request
     * @return mixed
     */
    public function store( Request $request );

    /**
     * Return parking space if exists in database
     *
     * @param Request $request
     * @return mixed
     */
    public function edit( Request $request );

    /**
     * Update the given parking space from database
     *
     * @param Request $request
     * @return mixed
     */
    public function update( Request $request );

    /**
     * Remove the given parking space from database
     *
     * @param Request $request
     * @return mixed
     */
    public function delete( Request $request );

}

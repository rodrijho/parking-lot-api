<?php namespace App\Http\Controllers\Access\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\Access\Auth\AuthInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * @var AuthInterface
     */
    private $auth;

    /**
     * Create a new controller instance.
     *
     * @param AuthInterface $auth
     */
    public function __construct( AuthInterface $auth )
    {
        $this->auth = $auth;
    }

    /**
     * Save a new user in database
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function register( Request $request )
    {
        $user = $this->auth->register( $request );

        return $this->makeResponse(
            true,
            'User ' . $request->first_name . ' created successfully',
            201,
            $user
        );
    }

    /**
     * Get a JWT token via credentials
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login( Request $request )
    {
        $login = $this->auth->login( $request );

        return $this->makeResponse(
            $login['success'],
            $login['message'],
            $login['code'],
            array_key_exists( 'data', $login ) == true ? $login['data'] : []
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getLoggedUser( Request $request )
    {
        $loggedUser = Auth::user();

        return $this->makeResponse(
            true,
            'User retrieved successfully',
            200,
            $loggedUser
        );
    }

    /**
     * Invalidate the token of the given user
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout( Request $request )
    {
        $this->auth->logout( $request );

        return $this->makeResponse(
            true,
            'User logged out successfully',
            200
        );
    }

}

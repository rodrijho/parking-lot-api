<?php namespace App\Listeners\Business;

use App\Events\Business\AttemptToParkEvent;
use App\Models\Business\ParkingLot\ParkingLot;
use App\Models\Business\ParkingLot\ParkingLotQueue;
use App\Models\Settings\ParkingSpace\ParkingSpace;
use App\Models\Settings\VehicleType\VehicleType;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AttemptToParkListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param AttemptToParkEvent $event
     * @return void
     */
    public function handle( AttemptToParkEvent $event )
    {
        $nextCarInQue = $event->nextCarInQue;

        if ( empty( $nextCarInQue ) == false )
        {
            DB::beginTransaction();

            try
            {
                $this->validateAndApplyChanges( $nextCarInQue );
            }
            catch (\Exception $exception)
            {
                DB::rollBack();

                Log::error(
                    'ParkingLotQueueListener.handle: Something went wrong handling the parking lot queue. '
                    . 'Details: ' . $exception->getMessage()
                );
            }

            DB::commit();
        }
        else
        {
            # Only take one from the in_use values
            if ( $event->parkingSpace->in_use - 1 >= 0 )
            {
                $event->parkingSpace->in_use = $event->parkingSpace->in_use - 1;
                $event->parkingSpace->save();
            }
        }

    }

    /**
     * Apply all the rules of the business
     *
     * @param ParkingLotQueue $next_car_in_queue
     * @return bool
     * @throws \Exception
     */
    private function validateAndApplyChanges( ParkingLotQueue $next_car_in_queue )
    {
        # Get the complete object to apply the validation
        $vehicleType  = VehicleType::query()
            ->where( 'id', $next_car_in_queue->vehicle_type_id )
            ->first();

        $parkingSpace = ParkingSpace::query()
            ->where( 'id', $next_car_in_queue->parking_space_id )
            ->first();

        # Parking spaces vehicle range
        $vehicleCanParkInSpaces = $vehicleType->parking_spaces->pluck( 'id' )->toArray();
        if ( in_array( $parkingSpace->id, array_values( $vehicleCanParkInSpaces ) ) == false )
        {
            # Update the parking space in_use field due to a car was removed from slot
            if ( empty( $parkingSpace ) == false )
            {
                # We don't want any negative number
                if ( $parkingSpace->in_use - 1 >= 0 )
                {
                    $parkingSpace->in_use = $parkingSpace->in_use - 1;
                    $parkingSpace->save();
                }

                Log::info(
                    'AttemptToParkListener.validateAndApplyChanges: Cannot park the car because ' .
                    ' it does not have a valid parking station. Car Details: ' . json_encode( $next_car_in_queue )
                );
            }
        }
        else
        {
            # There is a Parking Space in use in lot
            $parkingSpaceInLot = ParkingLot::query()
                ->where( 'parking_space_id', $next_car_in_queue->parking_space_id )
                ->first();

            if ( empty( $parkingSpaceInLot ) == true )
            {
                # Find the main resource
                $existingParkingSpace = ParkingSpace::query()
                    ->where( 'id', $next_car_in_queue->parking_space_id )
                    ->first();

                if ( empty( $existingParkingSpace ) == false )
                {
                    $existingParkingSpace->in_use = 1;
                    $existingParkingSpace->save();
                }
            }

            # The car is available to move to it's parking space and the amount of use remains exact
            $parkingLot = new ParkingLot();
            $parkingLot->vehicle_type_id    = $next_car_in_queue->vehicle_type_id;
            $parkingLot->parking_space_id   = $next_car_in_queue->parking_space_id;
            $parkingLot->created_by         = $next_car_in_queue->created_by;
            $parkingLot->save();

            Log::info(
                'AttemptToParkListener.validateAndApplyChanges: The car was moved to the required station. ' .
                'Car Details: ' . json_encode( $next_car_in_queue )
            );
        }

        $next_car_in_queue->delete();

        return true;
    }

}

<?php namespace App\Repositories\Access\Auth;

use Illuminate\Http\Request;

interface AuthInterface
{
    /**
     * Store a new user in database
     *
     * @param Request $request
     * @return mixed
     */
    public function register( Request $request );

    /**
     * Get a JWT token via credentials
     *
     * @param Request $request
     * @return mixed
     */
    public function login( Request $request );

    /**
     * Invalidate the token of the given user
     *
     * @param Request $request
     * @return mixed
     */
    public function logout( Request $request );

}

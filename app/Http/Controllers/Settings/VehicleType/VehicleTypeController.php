<?php namespace App\Http\Controllers\Settings\VehicleType;

use App\Http\Controllers\Controller;
use App\Repositories\Settings\VehicleType\VehicleTypeInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class VehicleTypeController extends Controller
{
    /**
     * @var VehicleTypeInterface
     */
    private $vehicleType;

    /**
     * VehicleTypeController constructor.
     * @param VehicleTypeInterface $vehicleType
     */
    public function __construct( VehicleTypeInterface $vehicleType )
    {
        $this->vehicleType = $vehicleType;
    }

    /**
     * Return the list of the available vehicles type
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index( Request $request )
    {
        $vehiclesType = $this->vehicleType->getAll( $request );

        return $this->makeResponse(
            true,
            'Vehicles type retrieved successfully',
            200,
            $vehiclesType
        );
    }

    /**
     * Store a new vehicle type in database
     *
     * @param Request $request
     * @return mixed
     */
    public function store( Request $request )
    {
        $this->vehicleType->store( $request );

        return $this->makeResponse(
            true,
            'The vehicle type was created successfully',
            201
        );
    }

    /**
     * Return the given vehicle type from database
     *
     * @param Request $request
     * @return mixed
     */
    public function edit( Request $request )
    {
        $vehicleType = $this->vehicleType->edit( $request );

        return $this->makeResponse(
            true,
            'Vehicle Type found',
            200,
            $vehicleType
        );
    }

    /**
     * Update the given vehicle type from database
     *
     * @param Request $request
     * @return mixed
     */
    public function update( Request $request )
    {
        $this->vehicleType->update( $request );

        return $this->makeResponse(
            true,
            'The vehicle type was updated successfully',
            200
        );
    }

    /**
     * Remove the given vehicle type from database
     *
     * @param Request $request
     * @return mixed
     */
    public function delete( Request $request )
    {
        $this->vehicleType->delete( $request );

        return $this->makeResponse(
            true,
            'The vehicle type was deleted successfully',
            200
        );
    }


}

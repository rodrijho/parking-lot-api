<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateParkingLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_lots', function (Blueprint $table)
        {
            $table->increments('id' );
            $table->integer('vehicle_type_id' )->nullable()->unsigned();
            $table->integer('parking_space_id' )->nullable()->unsigned();
            $table->integer('created_by' )->nullable()->unsigned();
            $table->timestamp('created_at' )->default( DB::raw('CURRENT_TIMESTAMP') );
            $table->timestamp('updated_at' )->nullable();

            // Relations
            $table->foreign('vehicle_type_id' )
                ->references('id' )
                ->on( config( 'variables.settings.vehicles_type.table' ) )
                ->onDelete( 'cascade' );

            $table->foreign('parking_space_id' )
                ->references('id' )
                ->on( config( 'variables.settings.parking_spaces.table' ) )
                ->onDelete( 'cascade' );

            $table->foreign('created_by' )
                ->references('id' )
                ->on( config( 'variables.access.auth.users.table' ) )
                ->onDelete( 'cascade' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists( 'parking_lots' );
        Schema::enableForeignKeyConstraints();
    }
}

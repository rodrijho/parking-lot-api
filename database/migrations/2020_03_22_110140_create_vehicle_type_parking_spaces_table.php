<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleTypeParkingSpacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_type_parking_spaces', function (Blueprint $table)
        {
            $table->integer('vehicle_type_id' )->nullable()->unsigned();
            $table->integer('parking_space_id' )->nullable()->unsigned();

            // Relations
            $table->foreign('vehicle_type_id' )
                ->references('id' )
                ->on( config( 'variables.settings.vehicles_type.table' ) )
                ->onDelete( 'cascade' );

            $table->foreign('parking_space_id' )
                ->references('id' )
                ->on( config( 'variables.settings.parking_spaces.table' ) )
                ->onDelete( 'cascade' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_type_parking_spaces');
    }
}

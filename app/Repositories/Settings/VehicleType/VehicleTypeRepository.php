<?php namespace App\Repositories\Settings\VehicleType;

use App\Exceptions\GeneralException;
use App\Models\Settings\VehicleType\VehicleType;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;

class VehicleTypeRepository implements VehicleTypeInterface
{
    const MODEL = VehicleType::class;

    /**
     * @inheritDoc
     * @throws GeneralException
     */
    public function getAll( Request $request, $order_by = 'name', $sort = 'asc' )
    {
        try
        {
            return VehicleType::query()
                ->select([
                    config('variables.settings.vehicles_type.table' ).'.id',
                    config('variables.settings.vehicles_type.table' ).'.name',
                    config('variables.settings.vehicles_type.table' ).'.description',
                    config('variables.settings.vehicles_type.table' ).'.active',
                    config('variables.settings.vehicles_type.table' ).'.created_by',
                    config('variables.settings.vehicles_type.table' ).'.created_at',
                    config('variables.settings.vehicles_type.table' ).'.updated_at',
                ])
                ->orderBy( $order_by, $sort )
                ->with( [ 'user', 'parking_spaces' ] )
                ->get();
        } catch ( Exception $exception )
        {
            Log::error(
                'VehicleTypeRepository.getAll: Something went wrong getting the list of vehicles type. ' .
                'Details: ' . $exception->getMessage()
            );

            throw new GeneralException( 'Something went wrong getting the list of vehicles type.' );
        }
    }

    /**
     * @inheritDoc
     * @param Request $request
     * @return bool
     * @throws GeneralException
     * @throws ValidationException
     */
    public function store( Request $request )
    {
        DB::beginTransaction();

        $this->makeRequestValidation( $request );

        try
        {
            $vehicleType = $this->createVehicleTypeStub( $request );
            $vehicleType->save();

            # Add the parking spaces relations
            $vehicleType->parking_spaces()->sync( $request->input( 'parking_spaces' ) );
        }
        catch ( Exception $exception )
        {
            DB::rollBack();

            Log::error(
                'VehicleTypeRepository.store: Something went wrong creating the vehicle type. Details: ' .
                $exception->getMessage()
            );

            throw new GeneralException(
                'Something went wrong creating the vehicle type.'
            );
        }

        DB::commit();

        return true;
    }

    /**
     * @inheritDoc
     * @param Request $request
     * @return Builder|Builder[]|Collection|Model
     */
    public function edit( Request $request )
    {
        return $this->getVehicleTypeById( $request->route( 'vehicle_type_id' ) );
    }

    /**
     * @inheritDoc
     * @param Request $request
     * @return bool
     * @throws GeneralException
     * @throws ValidationException
     */
    public function update( Request $request )
    {
        DB::beginTransaction();

        $vehicleType = $this->getVehicleTypeById( $request->route( 'vehicle_type_id' ) );

        $this->makeRequestValidation( $request, $vehicleType );

        try
        {
            $isActive = $request->input('active' );

            # Set the new values for the selected vehicle type
            $vehicleType->name             = $request->input('name' );
            $vehicleType->description      = $request->input('description' );
            $vehicleType->active           = is_null( $isActive ) == false ? $isActive : 1;
            $vehicleType->updated_at       = Carbon::now()->format( 'Y-m-d H:i:s' );
            $vehicleType->created_by       = Auth::user()->id;
            $vehicleType->save();

            # Add the parking spaces relations
            $vehicleType->parking_spaces()->sync( $request->input( 'parking_spaces' ) );
        }
        catch ( Exception $exception )
        {
            DB::rollBack();

            Log::error(
                'VehicleTypeRepository.update: Something went wrong updating the vehicle type. Details: ' .
                $exception->getMessage()
            );

            throw new GeneralException(
                'Something went wrong updating the vehicle type.'
            );
        }

        DB::commit();

        return true;
    }

    /**
     * @inheritDoc
     * @throws GeneralException
     */
    public function delete( Request $request )
    {
        $vehicleType = $this->getVehicleTypeById( $request->route( 'vehicle_type_id' ) );

        try
        {
            $vehicleType->delete();
            $vehicleType->parking_spaces()->delete();

            return true;
        }
        catch ( Exception $exception )
        {
            Log::error(
                'VehicleTypeRepository.delete: Something went wrong deleting the vehicle type. ' .
                'Details: ' . $exception->getMessage()
            );
            throw new GeneralException(
                'Something went wrong deleting the vehicle type, please try again ' .
                'later'
            );
        }
    }

    /**
     * Apply the validation to required fields for vehicle type
     *
     * @param Request $request
     * @param null $previous_vehicle_type
     * @return ValidationException|null
     * @throws ValidationException
     */
    private function makeRequestValidation( Request $request, $previous_vehicle_type = null ) : ?ValidationException
    {
        $uniqueValidation = is_null( $previous_vehicle_type ) == true ? 'unique:vehicles_type,name' :
            'unique:vehicles_type,name,' . $previous_vehicle_type->id;

        $validator = Validator::make(
            $request->all(),
            [
                'name'              => [ 'required', 'max:100', $uniqueValidation, 'regex:/^[A-ZÁÉÍÓÚa-záéíóú0-9_.,\s-]+$/i' ],
                'parking_spaces'    => [ 'required', 'array' ],
                'description'       => [ 'max:100', 'regex:/^[A-ZÁÉÍÓÚa-záéíóú0-9_.,\s-]+$/i' ],
            ]
        );

        if ( $validator->fails() )
        {
            throw ValidationException::withMessages( ( array ) $validator->getMessageBag() );
        }

        return null;
    }

    /**
     * @param Request $request
     * @return VehicleType
     */
    private function createVehicleTypeStub( Request $request ): VehicleType
    {
        $isActive = $request->input('active' );

        # New Parking Space Instance
        $vehicle_type_model            = self::MODEL;
        $vehicleType                   = new $vehicle_type_model();
        $vehicleType->name             = $request->input('name' );
        $vehicleType->description      = $request->input('description' );
        $vehicleType->active           = is_null( $isActive ) == false ? $isActive : 1;
        $vehicleType->created_by       = Auth::user()->id;

        return $vehicleType;
    }

    /**
     * @param $id
     * @return VehicleType|Builder|Builder[]|Collection|Model
     */
    private function getVehicleTypeById( $id ): VehicleType
    {
        return VehicleType::query()->with( [ 'user', 'parking_spaces' ] )->findOrFail( $id );
    }

}

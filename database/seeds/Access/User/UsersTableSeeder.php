<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        DB::table('users' )->truncate();

        DB::table('users' )->insert([
            'first_name'    => 'Parking',
            'last_name'     => 'Admin',
            'email'         => 'admin@admin.com',
            'gravatar'      => 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?f=y',
            'password'      => app('hash')->make('adminLot'),
            'created_at'    => Carbon::now()->format( 'Y-m-d H:i:s' ),
            'updated_at'    => Carbon::now()->format( 'Y-m-d H:i:s' )
        ]);

        Schema::enableForeignKeyConstraints();
    }
}

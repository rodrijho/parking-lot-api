<?php namespace App\Models\Settings\ParkingSpace\Traits\Relationship;

trait ParkingSpaceRelationship
{
    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(
            config('variables.access.auth.users.model' ),
            'created_by'
        );
    }

}

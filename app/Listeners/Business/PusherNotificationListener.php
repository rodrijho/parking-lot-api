<?php namespace App\Listeners\Business;

use App\Events\Business\PusherNotificationEvent;
use Illuminate\Support\Facades\Log;
use Pusher\Pusher;
use Pusher\PusherException;

class PusherNotificationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param PusherNotificationEvent $event
     * @return void
     */
    public function handle( PusherNotificationEvent $event )
    {
        try
        {
            $options = array(
                'cluster'   => env( 'PUSHER_APP_CLUSTER' ),
                'useTLS'    => env( 'PUSHER_APP_USE_TLS' )
            );

            $pusher = new Pusher(
                env( 'PUSHER_APP_KEY' ),
                env( 'PUSHER_APP_SECRET' ),
                env( 'PUSHER_APP_ID' ),
                $options
            );

            $pusher->trigger(
                'parking_lot',
                $event->notification_info['event'],
                $event->notification_info['data']
            );
        }
        catch (PusherException $pusherException)
        {
            Log::error(
                'PusherNotificationListener.handle.1: Something went wrong sending the notification. ' .
                'PusherException - Details: ' . $pusherException->getMessage()
            );
        }
        catch (\Exception $exception)
        {
            Log::error(
                'PusherNotificationListener.handle.2: Something went wrong sending the notification. ' .
                'GenericException - Details: ' . $exception->getMessage()
            );
        }
    }

}

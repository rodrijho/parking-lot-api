<?php namespace App\Models\Settings\VehicleType\Traits\Relationship;

trait VehicleTypeRelationship
{
    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(
            config('variables.access.auth.users.model' ),
            'created_by'
        );
    }

    /**
     * @return mixed
     */
    public function parking_spaces()
    {
        return $this->belongsToMany(
            config( 'variables.settings.parking_spaces.model' ),
            config( 'variables.settings.vehicles_type.pivot' )
        );
    }

}

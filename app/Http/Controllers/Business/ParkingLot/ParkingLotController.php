<?php namespace App\Http\Controllers\Business\ParkingLot;

use App\Http\Controllers\Controller;
use App\Repositories\Business\ParkingLot\ParkingLotInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ParkingLotController extends Controller
{
    /**
     * @var ParkingLotInterface
     */
    private $parkingLot;

    /**
     * ParkingLotController constructor.
     * @param ParkingLotInterface $parkingLot
     */
    public function __construct( ParkingLotInterface $parkingLot )
    {
        $this->parkingLot = $parkingLot;
    }

    /**
     * Return the list of the available parking lots
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index( Request $request )
    {
        $parkingLots = $this->parkingLot->getAll( $request );

        return $this->makeResponse(
            true,
            'Parking spaces retrieved successfully',
            200,
            $parkingLots
        );
    }

    /**
     * Store a new parking lot in database
     *
     * @param Request $request
     * @return mixed
     */
    public function store( Request $request )
    {
        $this->parkingLot->store( $request );

        return $this->makeResponse(
            true,
            'The parking lot was created successfully',
            201
        );
    }

    /**
     * Return the given parking lot from database
     *
     * @param Request $request
     * @return mixed
     */
    public function edit( Request $request )
    {
        $parkingLot = $this->parkingLot->edit( $request );

        return $this->makeResponse(
            true,
            'Parking Lot found',
            200,
            $parkingLot
        );
    }

    /**
     * Update the given parking lot from database
     *
     * @param Request $request
     * @return mixed
     */
    public function update( Request $request )
    {
        $this->parkingLot->update( $request );

        return $this->makeResponse(
            true,
            'The parking lot was updated successfully',
            200
        );
    }

    /**
     * Remove the given parking lot from database
     *
     * @param Request $request
     * @return mixed
     */
    public function delete( Request $request )
    {
        $this->parkingLot->delete( $request );

        return $this->makeResponse(
            true,
            'The parking lot was deleted successfully',
            200
        );
    }


}

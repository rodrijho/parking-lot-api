<?php

// API route group
$router->group([], function () use ($router)
{
    # Main access Routes
    $router->group(['prefix' => 'access', 'namespace' => 'Access' ], function () use ($router)
    {
        # Auth Routes
        $router->group(['prefix' => 'auth', 'namespace' => 'Auth', 'middleware' => [ 'json' ] ],
            function () use ($router)
        {
            $router->post('register', 'AuthController@register' );
            $router->post('login', 'AuthController@login' );
            $router->post('get-logged-user', [ 'middleware' => 'auth', 'uses' => 'AuthController@getLoggedUser' ] );
            $router->post('logout', [ 'middleware' => 'auth', 'uses' => 'AuthController@logout' ] );
        });

        # Profile Routes
        $router->group(['prefix' => 'profile', 'namespace' => 'Profile', 'middleware' => [ 'auth', 'json' ] ],
            function () use ($router)
            {
                $router->put('change-password', 'ProfileController@changePassword' );
                $router->put('info/update', 'ProfileController@updateProfile' );
            });

    }); # End of the Main Access Routes

                                    # PROTECTED ROUTES
    # Settings Routes
    $router->group(['prefix' => 'settings', 'namespace' => 'Settings', 'middleware' => [ 'auth', 'json' ] ],
        function () use ( $router )
        {
            # Parking Spaces Routes
            $router->group(['prefix' => 'parking-spaces', 'namespace' => 'ParkingSpace' ],
                function () use ( $router )
            {
                $router->get( 'index', 'ParkingSpaceController@index' );
                $router->post( 'store', 'ParkingSpaceController@store' );
                $router->get( '{parking_space_id}/edit', 'ParkingSpaceController@edit' );
                $router->put( '{parking_space_id}/update', 'ParkingSpaceController@update' );
                $router->delete( '{parking_space_id}/delete', 'ParkingSpaceController@delete' );
            }); # End of the Parking Spaces Routes

            # Vehicle Types Routes
            $router->group(['prefix' => 'vehicles-type', 'namespace' => 'VehicleType' ],
                function () use ( $router )
                {
                    $router->get( 'index', 'VehicleTypeController@index' );
                    $router->post( 'store', 'VehicleTypeController@store' );
                    $router->get( '{vehicle_type_id}/edit', 'VehicleTypeController@edit' );
                    $router->put( '{vehicle_type_id}/update', 'VehicleTypeController@update' );
                    $router->delete( '{vehicle_type_id}/delete', 'VehicleTypeController@delete' );
                }); # End of the Vehicles Type Routes

        }); # End of the Settings Routes

    # Business Routes
    $router->group(['prefix' => 'business', 'namespace' => 'Business', 'middleware' => [ 'auth', 'json' ] ],
        function () use ( $router )
        {
            # Parking Lot Routes
            $router->group(['prefix' => 'parking-lot', 'namespace' => 'ParkingLot' ],
                function () use ( $router )
            {
                $router->get( 'index', 'ParkingLotController@index' );
                $router->post( 'store', 'ParkingLotController@store' );
                $router->get( '{parking_lot_id}/edit', 'ParkingLotController@edit' );
                $router->delete( '{parking_lot_id}/delete', 'ParkingLotController@delete' );
            }); # End of the Parking Lot Routes

        }); # End of the Business Routes

});

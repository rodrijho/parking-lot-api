Parking Lot API
======================

[Parking Lot API](https://aws-parking-lot-api) is the app that expose the required endpoints
of the parking lot application. It serves the client the required information in order to
display it to the users. You can install it in your local environment and can adapt, extend, 
overwrite and customize anything to your needs.

## Table of content

- [Installation](#installation)
    - [Requirements](#Requirements)
    - [Composer](#Composer)
    - [Docker](#Docker)
- [Setup](#Setup)
    - [Database](#Database)
    - [Check Server](#Check_Server)

## Installation
    Download the project to your machine.
    This project contains the postman collection inside the documentation folder, so you can import the routes and test the endpoints with postman. 

### Requirements
- PHP >= 7.2
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Mysql >= 5.7
- Composer (Dependency Manager for PHP)
- Postman (Optional)

### Composer
- Please be sure you have [Composer](https://getcomposer.org) installed.
- Move to the project folder
- Run the command composer install
- Run the command php artisan jwt:secret to generate the API secret

### Docker
- Please be sure you have [Docker](https://runnable.com/docker/php/dockerize-your-laravel-application) installed.
- Move to the docker folder inside the project
- Set up the docker-compose.yml according your needs.
- Run the command docker-compose-up -d

## Setup
### Database
- Copy the .env.example file and name it .env
- Set up your database credentials
- Run the command php artisan migrate
- Run the command php artisan db:seed to mock the main tables

### Check_Server
- Please go to your [localhost server](http://localhost/path-to-your-project/public)
- [Download Postman Collection]( https://documenter.getpostman.com/view/3838871/SzS8sQFq)
- This project comes with a default Pusher credentials only for demo, you can set your own credentials if you want to.
- Enjoy....

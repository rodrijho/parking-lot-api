<?php namespace App\Listeners\Business;

use App\Events\Business\ParkingLotEvent;
use App\Events\Business\PusherNotificationEvent;
use App\Models\Business\ParkingLot\ParkingLot;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ParkingLotListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Save a new car in it's parking space
     *
     * Handle the event.
     *
     * @param ParkingLotEvent $event
     * @return array
     */
    public function handle( ParkingLotEvent $event )
    {
        DB::beginTransaction();

        try
        {
            $parkingSpaceInstance = $event->parking_space;

            $newValue = $event->action == 'ADD' ? $parkingSpaceInstance->in_use + 1 : $parkingSpaceInstance->in_use - 1;
            $parkingSpaceInstance->in_use = ( int ) $newValue ;
            $parkingSpaceInstance->save();

            $carsInLot = ParkingLot::query()->count();

            event(
                new PusherNotificationEvent([
                    'event' => 'new_car_in_lot',
                    'data'  => [ 'cars_in_lot' => $carsInLot ],
                ])
            );

        }
        catch ( \Exception $exception )
        {
            DB::rollBack();

            Log::error(
                'ParkingLotListener.handle: Something went wrong handling the new parking event. ' .
                'Details: ' . $exception->getMessage()
            );
        }

        DB::commit();

        return [
            'message' => null
        ];
    }
}

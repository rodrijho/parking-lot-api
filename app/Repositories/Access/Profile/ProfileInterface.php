<?php namespace App\Repositories\Access\Profile;

use Illuminate\Http\Request;

interface ProfileInterface
{
    /**
     * Change user password
     *
     * @param Request $request
     * @return mixed
     */
    public function changePassword( Request $request );

    /**
     * Update the user profile data
     *
     * @param Request $request
     * @return mixed
     */
    public function updateProfile( Request $request );

}

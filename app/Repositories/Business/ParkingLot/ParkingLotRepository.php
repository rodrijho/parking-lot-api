<?php namespace App\Repositories\Business\ParkingLot;

use App\Events\Business\AttemptToParkEvent;
use App\Events\Business\ParkingLotEvent;
use App\Events\Business\ParkingLotQueueEvent;
use App\Exceptions\GeneralException;
use App\Models\Business\ParkingLot\ParkingLot;
use App\Models\Settings\ParkingSpace\ParkingSpace;
use App\Models\Settings\VehicleType\VehicleType;
use App\Repositories\Settings\ParkingSpace\ParkingSpaceInterface;
use App\Repositories\Settings\VehicleType\VehicleTypeInterface;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;

class ParkingLotRepository implements ParkingLotInterface
{
    const MODEL = ParkingLot::class;
    /**
     * @var VehicleTypeInterface
     */
    private $vehicleType;

    /**
     * @var ParkingSpaceInterface
     */
    private $parkingSpace;

    /**
     * @var ParkingSpace
     */
    protected $notificationParkingSpace = null;

    /**
     * ParkingLotRepository constructor.
     * @param VehicleTypeInterface $vehicleType
     * @param ParkingSpaceInterface $parkingSpace
     */
    public function __construct( VehicleTypeInterface $vehicleType, ParkingSpaceInterface $parkingSpace )
    {
        $this->vehicleType = $vehicleType;
        $this->parkingSpace = $parkingSpace;
    }

    /**
     * @inheritDoc
     * @throws GeneralException
     */
    public function getAll( Request $request, $order_by = 'updated_at', $sort = 'desc' )
    {
        try
        {
            return ParkingLot::query()
                ->select([
                    config('variables.business.parking_lot.table' ).'.id',
                    config('variables.business.parking_lot.table' ).'.vehicle_type_id',
                    config('variables.business.parking_lot.table' ).'.parking_space_id',
                    config('variables.business.parking_lot.table' ).'.created_by',
                    config('variables.business.parking_lot.table' ).'.created_at',
                    config('variables.business.parking_lot.table' ).'.updated_at',
                ])
                ->orderBy( $order_by, $sort )
                ->with( [ 'vehicle_type', 'parking_space', 'created_by' ] )
                ->get();
        } catch ( Exception $exception )
        {
            Log::error(
                'ParkingLotRepository.getAll: Something went wrong getting the parking lot. ' .
                'Details: ' . $exception->getMessage()
            );

            throw new GeneralException( 'Something went wrong getting the the parking lot.' );
        }
    }

    /**
     * @inheritDoc
     * @param Request $request
     * @return bool
     * @throws GeneralException
     * @throws ValidationException
     */
    public function store( Request $request )
    {
        $this->makeRequestValidation( $request );

        DB::beginTransaction();

        try
        {
            $parkingLot = $this->createParkingLotStub( $request );
            $parkingLot->save();

            event( new ParkingLotEvent( $this->notificationParkingSpace, $parkingLot, 'ADD' ) );
        }
        catch ( Exception $exception )
        {
            DB::rollBack();

            Log::error(
                'ParkingLotRepository.store: Something went wrong creating the parking lot. Details: ' .
                $exception->getMessage()
            );

            throw new GeneralException(
                'Something went wrong creating the parking lot.'
            );
        }

        DB::commit();

        return true;
    }

    /**
     * @inheritDoc
     * @param Request $request
     * @return Builder|Builder[]|Collection|Model
     */
    public function edit( Request $request )
    {
        return $this->getParkingLotById( $request->route( 'parking_lot_id' ) );
    }

    /**
     * @inheritDoc
     * @throws GeneralException
     */
    public function delete( Request $request )
    {
        $parkingLot = $this->getParkingLotById( $request->route( 'parking_lot_id' ) );

        DB::beginTransaction();;

        try
        {
            event( new AttemptToParkEvent( $parkingLot->parking_space ) );

            $parkingLot->delete();
        }
        catch ( Exception $exception )
        {
            DB::rollBack();

            Log::error(
                'ParkingLotRepository.delete: Something went wrong deleting the parking lot. ' .
                'Details: ' . $exception->getMessage()
            );
            throw new GeneralException(
                'Something went wrong deleting the parking lot, please try again ' .
                'later'
            );
        }

        DB::commit();

        return true;
    }

    /**
     * Apply the validation to required fields for parking lot
     *
     * @param Request $request
     * @param string $action
     * @return ValidationException|null
     * @throws ValidationException
     */
    private function makeRequestValidation( Request $request, $action = 'ADD' ) : ?ValidationException
    {
        $vehicleTypeId  = $request->input('vehicle_type_id');
        $parkingSpaceId = $request->input('parking_space_id');

        $validator = Validator::make(
            $request->all(),
            [
                'vehicle_type_id'   => [ 'required', 'integer' ],
                'parking_space_id'  => [ 'required', 'integer'],
            ]
        );

        if ( $validator->fails() )
        {
            throw ValidationException::withMessages( ( array ) $validator->getMessageBag() );
        }

        $vehicleType  = VehicleType::query()->where( 'id', $vehicleTypeId )->first();
        $parkingSpace = ParkingSpace::query()->where( 'id', $parkingSpaceId )->first();

        if ( empty( $parkingSpace ) == true || empty( $vehicleType ) == true )
        {
            $error = [
                'messages' => [
                    0 => 'The parking space or the vehicle type does not exist, please try again.'
                ]
            ];

            throw ValidationException::withMessages( $error );
        }

        $this->notificationParkingSpace = $parkingSpace;

        $this->applyBusinessRules(
            $parkingSpace,
            $vehicleType,
            $action
        );

        return null;
    }

    /**
     * @param Request $request
     * @return ParkingLot
     */
    private function createParkingLotStub( Request $request ): ParkingLot
    {
        # New Parking Space Instance
        $parking_lot_model              = self::MODEL;
        $parkingLot                     = new $parking_lot_model();
        $parkingLot->vehicle_type_id    = $request->input('vehicle_type_id' );
        $parkingLot->parking_space_id   = $request->input('parking_space_id' );
        $parkingLot->created_by         = Auth::user()->id;

        return $parkingLot;
    }

    /**
     * @param $id
     * @return ParkingLot|Builder|Builder[]|Collection|Model
     */
    private function getParkingLotById( $id ): ParkingLot
    {
        return ParkingLot::query()->with( [ 'vehicle_type', 'parking_space', 'created_by' ] )->findOrFail( $id );
    }

    /**
     * Apply all the rules of the business
     *
     * @param ParkingSpace|Model $parking_space
     * @param VehicleType|Model $vehicle_type
     * @param string $action
     * @return ValidationException|null
     * @throws ValidationException
     */
    private function applyBusinessRules( $parking_space, $vehicle_type, string $action ) : ?ValidationException
    {
        # Check if the parking space has any available slot
        $newValue = $action == 'ADD' ? $parking_space->in_use + 1 : $parking_space->in_use - 1;
        if ( $newValue > $parking_space->parking_limit )
        {
            $queueInformation = [
                'vehicle_type_id'  => $vehicle_type->id,
                'parking_space_id' => $parking_space->id,
                'action'           => 'ADD',
                'created_by'       => Auth::user()->id
            ];

            # Call the other method to put the vehicle type in queue
            event( new ParkingLotQueueEvent( $queueInformation ) );

            $error = [
                'messages' => [
                    0 => 'The parking space does not have available slots, your request will be put on hold.'
                ]
            ];

            throw ValidationException::withMessages( $error );
        }

        # Parking spaces vehicle range
        $vehicleCanParkInSpaces = $vehicle_type->parking_spaces->pluck( 'id' )->toArray();
        if ( in_array( $parking_space->id, array_values( $vehicleCanParkInSpaces ) ) == false )
        {
            $error = [
                'messages' => [
                    0 => 'This type of vehicle cannot park in this spaces, please try again.'
                ]
            ];

            throw ValidationException::withMessages( $error );
        }

        return null;
    }

}

<?php namespace App\Models\Access\Traits\Attributes;

use Carbon\Carbon;

trait UserAttributes
{
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Accessor for getting full name of the user
    **/
    public function getFullNameAttribute(): string
    {
        return ucfirst( $this->first_name ) . ' ' . ucfirst( $this->last_name );
    }

    /**
     * Custom format for the created_at date
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute( $value ) : string
    {
        return Carbon::createFromFormat( 'Y-m-d H:i:s', $this->attributes['created_at'] )->diffForHumans();
    }

    /**
     * Custom format for the updated_at date
     *
     * @param $value
     * @return string
     */
    public function getUpdatedAtAttribute( $value ) : string
    {
        $dateToConvert = empty( $this->attributes['updated_at'] ) == false ? $this->attributes['updated_at'] :
            $this->attributes['created_at'];

        return Carbon::createFromFormat( 'Y-m-d H:i:s', $dateToConvert )->diffForHumans();
    }

}

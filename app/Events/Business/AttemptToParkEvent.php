<?php namespace App\Events\Business;

use App\Events\Event;
use App\Models\Business\ParkingLot\ParkingLotQueue;
use App\Models\Settings\ParkingSpace\ParkingSpace;
use Illuminate\Queue\SerializesModels;

class AttemptToParkEvent extends Event
{
    use SerializesModels;

    /**
     * @var ParkingLotQueue
     */
    public $nextCarInQue;

    /**
     * @var ParkingSpace
     */
    public $parkingSpace;

    /**
     * Create a new event instance.
     * @param ParkingSpace $parkingSpace
     */
    public function __construct( ParkingSpace $parkingSpace )
    {
        $nextCarInQueue = $availableCarsInQueue = ParkingLotQueue::query()
            ->orderBy( 'created_at', 'ASC' )
            ->first();

        $this->nextCarInQue = $nextCarInQueue;
        $this->parkingSpace = $parkingSpace;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

}

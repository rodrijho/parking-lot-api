<?php namespace App\Repositories\Access\Profile;

use App\Exceptions\GeneralException;
use App\Models\Access\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Tymon\JWTAuth\JWTAuth;

class ProfileRepository implements ProfileInterface
{
    const MODEL = User::class;

    /**
     * @var JWTAuth
     */
    protected $jwt;

    /**
     * JWTAuthController constructor.
     * @param JWTAuth $jwt
     */
    public function __construct( JWTAuth $jwt )
    {
        $this->jwt = $jwt;
    }

    /**
     * @inheritDoc
     * @throws GeneralException
     * @throws ValidationException
     */
    public function changePassword( Request $request )
    {
        DB::beginTransaction();

        $this->makeRequestValidation( $request, true );

        try
        {
            # Check for old password integrity
            if
            (
                Hash::check(
                    $request->input('old_password'),
                    Auth::user()->password
                ) == false
            )
            {
                $arrayResponse = [
                    'success' => false,
                    'message' => 'Your current password does not matches with the password you provided. Please try again.'
                ];
            }
            elseif ( strcmp ( $request->get('old_password' ), $request->get('new_password') ) == 0 )
            {
                # Check if passwords are the same
                $message = 'New Password cannot be same as your current password. Please choose a different password.';

                $arrayResponse = [
                    'success' => false,
                    'message' => $message
                ];
            }
            else
            {
                // Change Password
                $user = Auth::user();
                $user->password   = app('hash')->make( $request->input('new_password' ) );
                $user->updated_at = Carbon::now();
                $user->save();

                $arrayResponse = [
                    'success' => true,
                    'message' => 'Password changed successfully !'
                ];
            }
        }
        catch ( \Exception $exception )
        {
            DB::rollBack();

            Log::error(
                'AuthRepository.register: Something went wrong creating the new user. Details: ' .
                $exception->getMessage()
            );

            throw new GeneralException(
                'Something went wrong creating the user, please try again later'
            );
        }

        DB::commit();

        return $arrayResponse;
    }

    /**
     * @inheritDoc
     * @param Request $request
     * @throws GeneralException
     * @throws ValidationException
     */
    public function updateProfile( Request $request )
    {
        $this->makeRequestValidation( $request );

        try
        {
            $user = Auth::user();
            $user->first_name   = $request->input( 'first_name' );
            $user->last_name    = $request->input( 'last_name' );
            $user->email        = $request->input( 'email' );
            $user->phone_number = $request->input( 'phone' );
            $user->save();

            return [
                'success' => true,
                'message' => 'User profile has been updated !',
                'code'    => 200,
            ];
        }
        catch ( \Exception $exception )
        {
            Log::error(
                'ProfileRepository.updateProfile: Something went wrong trying to login the user. Details: ' .
                $exception->getMessage()
            );

            throw new GeneralException(
                'Something went wrong trying to update the user profile.'
            );
        }
    }

    /**
     * Run the request validation before process the information
     *
     * @param Request $request
     * @param bool $change_password
     * @return ValidationException|null
     * @throws ValidationException
     */
    private function makeRequestValidation( Request $request, $change_password = false ) : ?ValidationException
    {
        $validation = [
            'first_name'    => [ 'required', 'string', 'max:100' ],
            'last_name'     => [ 'required', 'string', 'max:100' ],
            'email'         => [ 'required', 'email', 'max:100', 'unique:users,email,' . Auth::user()->id ],
            'phone'         => [ 'max:20' ],
        ];

        if ( $change_password == true )
        {
            $validation = [
                'old_password'               => [ 'required', 'between:8,16' ],
                'new_password'               => [ 'required', 'between:8,16', 'confirmed' ],
                'new_password_confirmation'  => [ 'required', 'between:8,16' ],
            ];
        }

        $validator = Validator::make(
            $request->all(),
            $validation
        );

        if ( $validator->fails() )
        {
            throw ValidationException::withMessages( ( array ) $validator->getMessageBag() );
        }

        return null;
    }

}

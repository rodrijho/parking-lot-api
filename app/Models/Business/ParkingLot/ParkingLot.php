<?php namespace App\Models\Business\ParkingLot;

use App\Models\Business\ParkingLot\Traits\Attribute\ParkingLotAttribute;
use App\Models\Business\ParkingLot\Traits\Relationship\ParkingLotRelationship;
use Illuminate\Database\Eloquent\Model;

class ParkingLot extends Model
{
    use ParkingLotAttribute, ParkingLotRelationship;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vehicle_type_id',
        'parking_space_id',
        'created_by',
    ];

    /**
     * The fields that are going to be treated as dates
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_by',
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct( $attributes );

        $this->table = config('variables.business.parking_lot.table' );
    }

}

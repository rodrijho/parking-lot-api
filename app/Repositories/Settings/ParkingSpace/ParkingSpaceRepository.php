<?php namespace App\Repositories\Settings\ParkingSpace;

use App\Exceptions\GeneralException;
use App\Models\Settings\ParkingSpace\ParkingSpace;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;

class ParkingSpaceRepository implements ParkingSpaceInterface
{
    const MODEL = ParkingSpace::class;

    /**
     * @inheritDoc
     * @throws GeneralException
     */
    public function getAll( Request $request, $order_by = 'name', $sort = 'asc' )
    {
        try
        {
            return ParkingSpace::query()
                ->select([
                    config('variables.settings.parking_spaces.table' ).'.id',
                    config('variables.settings.parking_spaces.table' ).'.name',
                    config('variables.settings.parking_spaces.table' ).'.description',
                    config('variables.settings.parking_spaces.table' ).'.parking_limit',
                    config('variables.settings.parking_spaces.table' ).'.in_use',
                    config('variables.settings.parking_spaces.table' ).'.active',
                    config('variables.settings.parking_spaces.table' ).'.created_by',
                    config('variables.settings.parking_spaces.table' ).'.created_at',
                    config('variables.settings.parking_spaces.table' ).'.updated_at',
                ])
                ->orderBy( $order_by, $sort )
                ->with( 'user' )
                ->get();
        } catch ( Exception $exception )
        {
            Log::error(
                'ParkingSpaceRepository.getAll: Something went wrong getting the list of spaces. ' .
                'Details: ' . $exception->getMessage()
            );

            throw new GeneralException( 'Something went wrong getting the list of spaces.' );
        }
    }

    /**
     * @inheritDoc
     * @param Request $request
     * @return bool
     * @throws GeneralException
     * @throws ValidationException
     */
    public function store( Request $request )
    {
        DB::beginTransaction();

        $this->makeRequestValidation( $request );

        try
        {
            $parkingSpace = $this->createParkingSpaceStub( $request );
            $parkingSpace->save();
        }
        catch ( Exception $exception )
        {
            DB::rollBack();

            Log::error(
                'ParkingSpaceRepository.store: Something went wrong creating the parking space. Details: ' .
                $exception->getMessage()
            );

            throw new GeneralException(
                'Something went wrong creating the parking space.'
            );
        }

        DB::commit();

        return true;
    }

    /**
     * @inheritDoc
     * @param Request $request
     * @return Builder|Builder[]|Collection|Model
     */
    public function edit( Request $request )
    {
        return $this->getParkingSpaceById( $request->route( 'parking_space_id' ) );
    }

    /**
     * @inheritDoc
     * @param Request $request
     * @return bool
     * @throws GeneralException
     * @throws ValidationException
     */
    public function update( Request $request )
    {
        DB::beginTransaction();

        $parkingSpace = $this->getParkingSpaceById( $request->route( 'parking_space_id' ) );

        $this->makeRequestValidation( $request, $parkingSpace );

        try
        {
            $isActive = $request->input('active' );

            # Set the new values for the selected parking state
            $parkingSpace->name             = $request->input('name' );
            $parkingSpace->description      = $request->input('description' );
            $parkingSpace->parking_limit    = $request->input('parking_limit' );
            $parkingSpace->active           = is_null( $isActive ) == false ? $isActive : 1;
            $parkingSpace->created_by       = Auth::user()->id;
            $parkingSpace->save();
        }
        catch ( Exception $exception )
        {
            DB::rollBack();

            Log::error(
                'ParkingSpaceRepository.update: Something went wrong updating the parking space. Details: ' .
                $exception->getMessage()
            );

            throw new GeneralException(
                'Something went wrong updating the parking space.'
            );
        }

        DB::commit();

        return true;
    }

    /**
     * @inheritDoc
     * @throws GeneralException
     */
    public function delete( Request $request )
    {
        $parkingSpace = $this->getParkingSpaceById( $request->route( 'parking_space_id' ) );

        try
        {
            $parkingSpace->delete();

            return true;
        }
        catch ( Exception $exception )
        {
            Log::error(
                'ParkingSpaceRepository.delete: Something went wrong deleting the parking space. ' .
                'Details: ' . $exception->getMessage()
            );
            throw new GeneralException(
                'Something went wrong deleting the parking space, please try again ' .
                'later'
            );
        }
    }

    /**
     * Apply the validation to required fields for parking spaces
     *
     * @param Request $request
     * @param null|ParkingSpace $previous_parking_space
     * @return ValidationException|null
     * @throws ValidationException
     */
    private function makeRequestValidation( Request $request, $previous_parking_space = null ) : ?ValidationException
    {
        $uniqueValidation = is_null( $previous_parking_space ) == true ? 'unique:parking_spaces,name' :
            'unique:parking_spaces,name,' . $previous_parking_space->id;

        $validator = Validator::make(
            $request->all(),
            [
                'name'          => [ 'required', 'max:100', $uniqueValidation, 'regex:/^[A-ZÁÉÍÓÚa-záéíóú0-9_.,\s-]+$/i' ],
                'description'   => [ 'max:100', 'regex:/^[A-ZÁÉÍÓÚa-záéíóú0-9_.,\s-]+$/i' ],
                'parking_limit' => [ 'required', 'integer', 'between:1,50' ],
            ]
        );

        if ( $validator->fails() )
        {
            throw ValidationException::withMessages( ( array ) $validator->getMessageBag() );
        }

        return null;
    }

    /**
     * @param Request $request
     * @return ParkingSpace
     */
    private function createParkingSpaceStub( Request $request ): ParkingSpace
    {
        $isActive = $request->input('active' );

        # New Parking Space Instance
        $parking_space_model            = self::MODEL;
        $parkingSpace                   = new $parking_space_model();
        $parkingSpace->name             = $request->input('name' );
        $parkingSpace->description      = $request->input('description' );
        $parkingSpace->parking_limit    = $request->input('parking_limit' );
        $parkingSpace->active           = is_null( $isActive ) == false ? $isActive : 1;
        $parkingSpace->created_by       = Auth::user()->id;

        return $parkingSpace;
    }

    /**
     * @param $id
     * @return ParkingSpace|Builder|Builder[]|Collection|Model
     */
    private function getParkingSpaceById( $id ): ParkingSpace
    {
        return ParkingSpace::query()->with( 'user' )->findOrFail( $id );
    }

}

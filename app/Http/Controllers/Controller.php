<?php namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * Generic response that each controller will be able to use to generate their responses
     *
     * @param $success
     * @param $message
     * @param $data
     * @param $code
     * @return JsonResponse
     */
    public function makeResponse( $success, $message, $code, $data = [] )
    {
        if ( empty( $data ) == false )
        {
            return response()->json([
                'success' => $success,
                'message' => $message,
                'data'    => $data,
            ], $code );
        }

        return response()->json([
            'success' => $success,
            'message' => $message
        ], $code );
    }

}

<?php namespace App\Http\Controllers\Access\Profile;

use App\Http\Controllers\Controller;
use App\Repositories\Access\Profile\ProfileInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * @var ProfileInterface
     */
    private $profile;

    /**
     * Create a new controller instance.
     *
     * @param ProfileInterface $profile
     */
    public function __construct( ProfileInterface $profile )
    {
        $this->profile = $profile;
    }

    /**
     * Change the user credentials
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function changePassword( Request $request )
    {
        $changePassword = $this->profile->changePassword( $request );

        return $this->makeResponse(
            $changePassword['success'],
            $changePassword['message'],
            $changePassword['success'] == true ? 200 : 400,
            array_key_exists( 'data', $changePassword ) == true ? $changePassword['data'] : []
        );
    }

    /**
     * Change the user credentials
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateProfile( Request $request )
    {
        $updateProfile = $this->profile->updateProfile( $request );

        return $this->makeResponse(
            $updateProfile['success'],
            $updateProfile['message'],
            $updateProfile['success'] == true ? 200 : 400,
            array_key_exists( 'data', $updateProfile ) == true ? $updateProfile['data'] : []
        );
    }

}

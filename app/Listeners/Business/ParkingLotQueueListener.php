<?php namespace App\Listeners\Business;

use App\Events\Business\ParkingLotQueueEvent;
use App\Events\Business\PusherNotificationEvent;
use App\Models\Business\ParkingLot\ParkingLotQueue;
use Illuminate\Support\Facades\Log;

class ParkingLotQueueListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ParkingLotQueueEvent $event
     * @return void
     */
    public function handle( ParkingLotQueueEvent $event )
    {
        $queue_information = $event->queue_info;

        try
        {
            $parkingLotQueue                   = new ParkingLotQueue();
            $parkingLotQueue->vehicle_type_id  = $queue_information['vehicle_type_id'];
            $parkingLotQueue->parking_space_id = $queue_information['parking_space_id'];
            $parkingLotQueue->created_by       = $queue_information['created_by'];
            $parkingLotQueue->save();

            $carsInQueue = ParkingLotQueue::query()->count();

            event(
                new PusherNotificationEvent([
                    'event' => 'new_car_in_queue',
                    'data'  => [ 'cars_in_queue' => $carsInQueue ],
                ])
            );
        }
        catch (\Exception $exception)
        {
            Log::error(
                'ParkingLotQueueListener.handle: Something went wrong handling the parking lot queue. '
                . 'Details: ' . $exception->getMessage()
            );
        }
    }
}

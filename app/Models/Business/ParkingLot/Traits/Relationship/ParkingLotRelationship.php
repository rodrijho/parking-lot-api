<?php namespace App\Models\Business\ParkingLot\Traits\Relationship;

trait ParkingLotRelationship
{
    /**
     * @return mixed
     */
    public function vehicle_type()
    {
        return $this->belongsTo(
            config( 'variables.settings.vehicles_type.model' ),
            'vehicle_type_id'
        );
    }

    /**
     * @return mixed
     */
    public function parking_space()
    {
        return $this->belongsTo(
            config( 'variables.settings.parking_spaces.model' ),
            'parking_space_id'
        );
    }

    /**
     * @return mixed
     */
    public function created_by()
    {
        return $this->belongsTo(
            config( 'variables.access.auth.users.model' ),
            'created_by'
        );
    }

}

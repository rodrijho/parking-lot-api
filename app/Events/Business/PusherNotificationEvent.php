<?php namespace App\Events\Business;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class PusherNotificationEvent extends Event
{
    use SerializesModels;

    /**
     * @var array
     */
    public $notification_info;

    /**
     * Create a new event instance.
     *
     * @param array $notification_info
     */
    public function __construct( array $notification_info )
    {
        $this->notification_info = $notification_info;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

}

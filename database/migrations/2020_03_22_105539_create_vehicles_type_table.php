<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles_type', function (Blueprint $table)
        {
            $table->increments('id' );
            $table->string( 'name', 100 )->unique();
            $table->string( 'description', 150 )->nullable();
            $table->tinyInteger( 'active' )->default( true );
            $table->integer('created_by' )->nullable()->unsigned();
            $table->timestamp('created_at' )->default( DB::raw('CURRENT_TIMESTAMP') );
            $table->timestamp('updated_at' )->nullable();

            // Relations
            $table->foreign('created_by' )
                ->references('id' )
                ->on( config( 'variables.access.auth.users.table' ) )
                ->onDelete( 'cascade' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists( 'vehicles_type' );
        Schema::enableForeignKeyConstraints();
    }
}

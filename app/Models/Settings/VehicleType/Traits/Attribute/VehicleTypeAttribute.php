<?php namespace App\Models\Settings\VehicleType\Traits\Attribute;

use Carbon\Carbon;

trait VehicleTypeAttribute
{
    /**
     * Custom format for the created_at date
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute( $value )
    {
        return Carbon::createFromFormat( 'Y-m-d H:i:s', $this->attributes['created_at'] )->diffForHumans();
    }

    /**
     * Custom format for the updated_at date
     *
     * @param $value
     * @return string
     */
    public function getUpdatedAtAttribute( $value )
    {
        $dateToConvert = empty( $this->attributes['updated_at'] ) == false ? $this->attributes['updated_at'] :
            $this->attributes['created_at'];

        return Carbon::createFromFormat( 'Y-m-d H:i:s', $dateToConvert )->diffForHumans();
    }

}

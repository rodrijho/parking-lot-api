<?php namespace App\Repositories\Settings\VehicleType;

use Illuminate\Http\Request;

interface VehicleTypeInterface
{
    /**
     * Return the list of the available vehicle types
     *
     * @param Request $request
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getAll( Request $request, $order_by = 'name', $sort = 'asc' );

    /**
     * Store a new vehicle type in database
     *
     * @param Request $request
     * @return mixed
     */
    public function store( Request $request );

    /**
     * Return vehicle type if exists in database
     *
     * @param Request $request
     * @return mixed
     */
    public function edit( Request $request );

    /**
     * Update the given vehicle type from database
     *
     * @param Request $request
     * @return mixed
     */
    public function update( Request $request );

    /**
     * Remove the given vehicle type from database
     *
     * @param Request $request
     * @return mixed
     */
    public function delete( Request $request );

}

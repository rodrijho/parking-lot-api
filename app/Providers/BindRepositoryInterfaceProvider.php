<?php namespace App\Providers;

use App\Repositories\Access\Auth\AuthInterface;
use App\Repositories\Access\Auth\AuthRepository;
use App\Repositories\Access\Profile\ProfileInterface;
use App\Repositories\Access\Profile\ProfileRepository;
use App\Repositories\Business\ParkingLot\ParkingLotInterface;
use App\Repositories\Business\ParkingLot\ParkingLotRepository;
use App\Repositories\Settings\ParkingSpace\ParkingSpaceInterface;
use App\Repositories\Settings\ParkingSpace\ParkingSpaceRepository;
use App\Repositories\Settings\VehicleType\VehicleTypeInterface;
use App\Repositories\Settings\VehicleType\VehicleTypeRepository;
use Illuminate\Support\ServiceProvider;

class BindRepositoryInterfaceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind( AuthInterface::class, AuthRepository::class );
        $this->app->bind( ParkingSpaceInterface::class, ParkingSpaceRepository::class );
        $this->app->bind( VehicleTypeInterface::class, VehicleTypeRepository::class );
        $this->app->bind( ParkingLotInterface::class, ParkingLotRepository::class );
        $this->app->bind( ProfileInterface::class, ProfileRepository::class );
    }
}

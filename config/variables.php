<?php

use App\Models\Access\User;
use App\Models\Business\ParkingLot\ParkingLot;
use App\Models\Business\ParkingLot\ParkingLotQueue;
use App\Models\Settings\ParkingSpace\ParkingSpace;
use App\Models\Settings\VehicleType\VehicleType;
use App\Models\Settings\VehicleType\VehicleTypePivot;

return [

    'access' => [
        'auth' => [
            'users' => [
                'model' => User::class,
                'table' => 'users',
            ]
        ]
    ],

    'settings' => [

        'parking_spaces' => [
            'model' => ParkingSpace::class,
            'table' => 'parking_spaces',
        ],

        'vehicles_type' => [
            'model'       => VehicleType::class,
            'table'       => 'vehicles_type',
            'pivot'       => 'vehicle_type_parking_spaces',
            'pivot_model' => VehicleTypePivot::class,
        ],

    ],

    'business' => [

        'parking_lot' => [
            'model' => ParkingLot::class,
            'table' => 'parking_lots',
        ],

        'parking_lot_queue' => [
            'model' => ParkingLotQueue::class,
            'table' => 'parking_lot_queue',
        ],

    ],

];

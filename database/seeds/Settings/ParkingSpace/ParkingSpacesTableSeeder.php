<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ParkingSpacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        DB::table( config( 'variables.settings.parking_spaces.table' ) )->truncate();

        DB::table( config( 'variables.settings.parking_spaces.table' ) )->insert(
            [
                [
                    'name'          => 'Small',
                    'description'   => 'Small Space',
                    'parking_limit' => 5,
                    'in_use'        => 0,
                    'active'        => 1,
                    'created_by'    => 1,
                    'created_at'    => Carbon::now()->format( 'Y-m-d H:i:s' ),
                    'updated_at'    => Carbon::now()->format( 'Y-m-d H:i:s' )
                ], # Small Space
                [
                    'name'          => 'Medium',
                    'description'   => 'Medium Space',
                    'parking_limit' => 10,
                    'in_use'        => 0,
                    'active'        => 1,
                    'created_by'    => 1,
                    'created_at'    => Carbon::now()->format( 'Y-m-d H:i:s' ),
                    'updated_at'    => Carbon::now()->format( 'Y-m-d H:i:s' )
                ],  # Medium Space
                [
                    'name'          => 'Large',
                    'description'   => 'Large Space',
                    'parking_limit' => 15,
                    'in_use'        => 0,
                    'active'        => 1,
                    'created_by'    => 1,
                    'created_at'    => Carbon::now()->format( 'Y-m-d H:i:s' ),
                    'updated_at'    => Carbon::now()->format( 'Y-m-d H:i:s' )
                ],  # Large Space
            ]
        );

        Schema::enableForeignKeyConstraints();
    }
}

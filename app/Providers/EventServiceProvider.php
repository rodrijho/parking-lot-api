<?php namespace App\Providers;

use App\Events\Business\AttemptToParkEvent;
use App\Events\Business\ParkingLotEvent;
use App\Events\Business\ParkingLotQueueEvent;
use App\Events\Business\PusherNotificationEvent;
use App\Listeners\Business\AttemptToParkListener;
use App\Listeners\Business\ParkingLotListener;
use App\Listeners\Business\ParkingLotQueueListener;
use App\Listeners\Business\PusherNotificationListener;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ParkingLotEvent::class => [
            ParkingLotListener::class,
        ],
        ParkingLotQueueEvent::class => [
            ParkingLotQueueListener::class,
        ],
        AttemptToParkEvent::class => [
            AttemptToParkListener::class,
        ],
        PusherNotificationEvent::class => [
            PusherNotificationListener::class,
        ],
    ];
}

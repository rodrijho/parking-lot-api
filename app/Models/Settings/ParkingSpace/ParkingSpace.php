<?php namespace App\Models\Settings\ParkingSpace;

use App\Models\Settings\ParkingSpace\Traits\Attribute\ParkingSpaceAttribute;
use App\Models\Settings\ParkingSpace\Traits\Relationship\ParkingSpaceRelationship;
use Illuminate\Database\Eloquent\Model;

class ParkingSpace extends Model
{
    use ParkingSpaceAttribute, ParkingSpaceRelationship;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'parking_limit',
        'in_use',
        'active',
        'created_by',
    ];

    /**
     * The fields that are going to be treated as dates
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_by',
    ];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct( $attributes );

        $this->table = config('variables.settings.parking_spaces.table' );
    }

}

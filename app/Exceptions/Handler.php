<?php namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Throwable $exception
     * @return void
     *
     * @throws Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  Request  $request
     * @param Throwable $exception
     * @return JsonResponse|Response|\Symfony\Component\HttpFoundation\Response
     *
     * @throws Throwable
     */
    public function render( $request, Throwable $exception )
    {
        if ( $exception instanceof NotFoundHttpException )
            return response()->json( [
                'success' => false,
                'message' => 'The resource you are looking for, does not exists.'
            ], 404 );


        if ( $exception instanceof GeneralException )
        {
            if ( $exception->getCode() == 0 )
            {
                return \response()->json([
                    'success' => false,
                    'message' => 'Something went executing your request, please try again later.'
                ], 500 );
            }
        }

        if ( $exception instanceof MethodNotAllowedHttpException )
            return response()->json( [
                'success' => false,
                'message' => 'This route does not support the method you are using.'
            ], 405 );

        if ( $exception instanceof ModelNotFoundException )
            return response()->json( [
                'success' => false,
                'message' => 'The resource you are looking for, does not exists.'
            ], 404 );

        if ( $exception instanceof ValidationException )
        {
            $validationErrors = $exception->errors();
            $customErrors     = [];

            if ( is_array( $validationErrors ) && count( $validationErrors ) > 0 )
            {
                foreach ( $validationErrors as $error )
                {
                    foreach ( $error as $index => $value )
                    {
                        if ( $value == ':message' )
                        {
                            continue;
                        }

                        array_push(
                            $customErrors,
                            is_array( $value ) == true && array_key_exists( '0', $value ) == true
                                ? $value[0] : $value
                        );
                    }
                }
            }

            return response()->json( [
                'success' => false,
                'message' => $exception->getMessage(),
                'errors'  => $customErrors
            ], 422 );
        }

        return parent::render( $request, $exception );
    }
}

<?php namespace App\Events\Business;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class ParkingLotQueueEvent extends Event
{
    use SerializesModels;

    /**
     * @var array
     */
    public $queue_info;

    /**
     * Create a new event instance.
     *
     * @param array $queue_info
     */
    public function __construct( array $queue_info )
    {
        $this->queue_info = $queue_info;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

}

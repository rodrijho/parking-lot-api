<?php namespace App\Repositories\Business\ParkingLot;

use Illuminate\Http\Request;

interface ParkingLotInterface
{
    /**
     * Return the list of the available parking lots
     *
     * @param Request $request
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getAll( Request $request, $order_by = 'updated_at', $sort = 'desc' );

    /**
     * Store a new parking lot in database
     *
     * @param Request $request
     * @return mixed
     */
    public function store( Request $request );

    /**
     * Return parking lot if exists in database
     *
     * @param Request $request
     * @return mixed
     */
    public function edit( Request $request );

    /**
     * Remove the given parking lot from database
     *
     * @param Request $request
     * @return mixed
     */
    public function delete( Request $request );

}

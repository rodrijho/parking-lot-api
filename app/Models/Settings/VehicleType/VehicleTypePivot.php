<?php namespace App\Models\Settings\VehicleType;

use Illuminate\Database\Eloquent\Model;

class VehicleTypePivot extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vehicle_type_id',
        'parking_space_id',
    ];

    /**
     * The fields that are going to be treated as dates
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct( $attributes );

        $this->table = config('variables.settings.vehicles_type.pivot' );
    }

}

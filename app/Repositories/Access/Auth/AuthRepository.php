<?php namespace App\Repositories\Access\Auth;

use App\Exceptions\GeneralException;
use App\Models\Access\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Tymon\JWTAuth\JWTAuth;

class AuthRepository implements AuthInterface
{
    const MODEL = User::class;

    /**
     * @var JWTAuth
     */
    protected $jwt;

    /**
     * JWTAuthController constructor.
     * @param JWTAuth $jwt
     */
    public function __construct( JWTAuth $jwt )
    {
        $this->jwt = $jwt;
    }

    /**
     * @inheritDoc
     * @throws GeneralException
     * @throws ValidationException
     */
    public function register( Request $request )
    {
        DB::beginTransaction();

        $this->makeRequestValidation( $request );

        try
        {
            $emailHash   = md5( strtolower( trim( $request->input('email' ) ) ) );
            $gravatarUrl = "https://www.gravatar.com/avatar/" . $emailHash . "?s=150";

            $user_model         = self::MODEL;
            $user               = new $user_model();
            $user->first_name   = $request->input('first_name' );
            $user->last_name    = $request->input('last_name' );
            $user->email        = $request->input('email' );
            $user->gravatar     = $gravatarUrl;
            $plainPassword      = $request->input('password' );
            $user->password     = app('hash')->make( $plainPassword );

            $user->save();

            # Automatically Login The User
            $credentials = $request->only(['email', 'password']);
            $token = $this->jwt->attempt( $credentials );

            if ( $token == false )
            {
                Log::info( 'AuthRepository.login: Invalid credentials' );

                return [
                    'success' => false,
                    'message' => 'Unauthorized',
                    'code'    => 401
                ];
            }

            $loginInformation = [
                'token'      => $token,
                'token_type' => 'bearer',
                'expires_in' => Auth::factory()->getTTL() * 60,
                'user'       => [
                    'first_name'    => $user->first_name,
                    'last_name'     => $user->last_name,
                    'created_at'    => $user->created_at,
                    'gravatar'      => $user->gravatar,
                    'phone_number'  => $user->phone_number,
                    'email'         => $user->email,
                ]
            ];

        }
        catch ( \Exception $exception )
        {
            DB::rollBack();

            Log::error(
                'AuthRepository.register: Something went wrong creating the new user. Details: ' .
                $exception->getMessage()
            );

            throw new GeneralException(
                'Something went wrong creating the user, please try again later'
            );
        }

        DB::commit();

        return $loginInformation;
    }

    /**
     * @inheritDoc
     * @param Request $request
     * @throws GeneralException
     * @throws ValidationException
     */
    public function login( Request $request )
    {
        $this->makeLoginRequestValidation( $request );

        try
        {
            $user = User::query()->where( 'email', $request->email )->first();

            if ( empty( $user ) == true )
            {
                Log::info( 'AuthRepository.login: User do not exist in our database. Email: ' . $request->email );

                return [
                    'success' => false,
                    'message' => 'Invalid Credentials',
                    'code'    => 401
                ];
            }

            # Authenticate with the given credentials
            $credentials = $request->only(['email', 'password']);
            $token       = $this->jwt->attempt( $credentials );

            if ( $token == false )
            {
                Log::info( 'AuthRepository.login: Invalid credentials' );

                return [
                    'success' => false,
                    'message' => 'Invalid Credentials',
                    'code'    => 401
                ];
            }

            $loginInformation = [
                'token'      => $token,
                'token_type' => 'bearer',
                'expires_in' => Auth::factory()->getTTL() * 60,
                'user'       => [
                    'first_name'    => $user->first_name,
                    'last_name'     => $user->last_name,
                    'created_at'    => $user->created_at,
                    'gravatar'      => $user->gravatar,
                    'phone_number'  => $user->phone_number,
                    'email'         => $user->email,
                ]
            ];

            return [
                'success' => true,
                'message' => 'User logged in successfully',
                'code'    => 200,
                'data'    => $loginInformation
            ];
        }
        catch ( \Exception $exception )
        {
            Log::error(
                'AuthRepository.login: Something went wrong trying to login the user. Details: ' .
                $exception->getMessage()
            );

            throw new GeneralException(
                'Something went wrong trying to login the user.'
            );
        }
    }

    /**
     * @inheritDoc
     * @throws GeneralException
     */
    public function logout( Request $request )
    {
        try
        {
            $this->jwt->parseToken()->invalidate();

            return true;
        }
        catch ( \Exception $exception )
        {
            Log::error(
                'AuthRepository.logout: Something went wrong trying to logout the user. Details: ' .
                $exception->getMessage()
            );

            throw new GeneralException( 'Something went wrong trying to logout the user.' );
        }
    }

    /**
     * Run the request validation before process the information
     *
     * @param Request $request
     * @return ValidationException|null
     * @throws ValidationException
     */
    private function makeRequestValidation( Request $request ) : ?ValidationException
    {
        $validator = Validator::make(
            $request->all(),
            [
                'first_name'    => [ 'required', 'string', 'max:100' ],
                'last_name'     => [ 'required', 'string', 'max:100' ],
                'email'         => [ 'required', 'email', 'unique:users', 'max:100' ],
                'password'      => [ 'required', 'between:8,16' ],
            ]
        );

        if ( $validator->fails() )
        {
            throw ValidationException::withMessages( ( array ) $validator->getMessageBag() );
        }

        return null;
    }

    /**
     * Apply the validation to email and password
     *
     * @param Request $request
     * @return ValidationException|null
     * @throws ValidationException
     */
    private function makeLoginRequestValidation( Request $request ) : ?ValidationException
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email'         => [ 'required', 'email', 'string', 'max:100' ],
                'password'      => [ 'required', 'between:8,16' ],
            ]
        );

        if ( $validator->fails() )
        {
            throw ValidationException::withMessages( ( array ) $validator->getMessageBag() );
        }

        return null;
    }

}

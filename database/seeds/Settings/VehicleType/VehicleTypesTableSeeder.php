<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class VehicleTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        DB::table( config( 'variables.settings.vehicles_type.table' ) )->truncate();
        DB::table( config( 'variables.settings.vehicles_type.pivot' ) )->truncate();

        DB::table( config( 'variables.settings.vehicles_type.table' ) )->insert(
            [
                [
                    'name'              => 'Motorcycle',
                    'description'       => 'Small Vehicle',
                    'active'            => 1,
                    'created_by'        => 1,
                    'created_at'        => Carbon::now()->format( 'Y-m-d H:i:s' ),
                    'updated_at'        => Carbon::now()->format( 'Y-m-d H:i:s' )
                ], # Motorcycle Vehicle
                [
                    'name'              => 'Sedan',
                    'description'       => 'Medium Vehicle',
                    'active'            => 1,
                    'created_by'        => 1,
                    'created_at'        => Carbon::now()->format( 'Y-m-d H:i:s' ),
                    'updated_at'        => Carbon::now()->format( 'Y-m-d H:i:s' )
                ], # Sedan Vehicle
                [
                    'name'              => 'Truck',
                    'description'       => 'Large Vehicle',
                    'active'            => 1,
                    'created_by'        => 1,
                    'created_at'        => Carbon::now()->format( 'Y-m-d H:i:s' ),
                    'updated_at'        => Carbon::now()->format( 'Y-m-d H:i:s' )
                ], # Truck Vehicle
            ]
        );

        DB::table( config( 'variables.settings.vehicles_type.pivot' ) )->insert(
            [
                # Motorcycle can park in all spaces
                [
                    'vehicle_type_id'  => 1,
                    'parking_space_id' => 1,
                ],
                [
                    'vehicle_type_id'  => 1,
                    'parking_space_id' => 2,
                ],
                [
                    'vehicle_type_id'  => 1,
                    'parking_space_id' => 3,
                ],

                # Sedans can park only in medium and large spaces
                [
                    'vehicle_type_id'  => 2,
                    'parking_space_id' => 2,
                ],
                [
                    'vehicle_type_id'  => 2,
                    'parking_space_id' => 3,
                ],

                # Trucks can park only in large spaces
                [
                    'vehicle_type_id'  => 3,
                    'parking_space_id' => 3,
                ],
            ]
        );

        Schema::enableForeignKeyConstraints();
    }
}

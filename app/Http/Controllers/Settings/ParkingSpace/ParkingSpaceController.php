<?php namespace App\Http\Controllers\Settings\ParkingSpace;

use App\Http\Controllers\Controller;
use App\Repositories\Settings\ParkingSpace\ParkingSpaceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ParkingSpaceController extends Controller
{
    /**
     * @var ParkingSpaceInterface
     */
    private $parkingSpace;

    /**
     * Create a new controller instance.
     *
     * @param ParkingSpaceInterface $parkingSpace
     */
    public function __construct( ParkingSpaceInterface $parkingSpace )
    {
        $this->parkingSpace = $parkingSpace;
    }

    /**
     * Return the list of the available parking spaces
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index( Request $request )
    {
        $parkingSpaces = $this->parkingSpace->getAll( $request );

        return $this->makeResponse(
            true,
            'Parking spaces retrieved successfully',
            200,
            $parkingSpaces
        );
    }

    /**
     * Store a new parking space in database
     *
     * @param Request $request
     * @return mixed
     */
    public function store( Request $request )
    {
        $this->parkingSpace->store( $request );

        return $this->makeResponse(
            true,
            'The parking space was created successfully',
            201
        );
    }

    /**
     * Return the given parking space from database
     *
     * @param Request $request
     * @return mixed
     */
    public function edit( Request $request )
    {
        $parkingSpace = $this->parkingSpace->edit( $request );

        return $this->makeResponse(
            true,
            'Parking Space found',
            200,
            $parkingSpace
        );
    }

    /**
     * Update the given parking space from database
     *
     * @param Request $request
     * @return mixed
     */
    public function update( Request $request )
    {
        $this->parkingSpace->update( $request );

        return $this->makeResponse(
            true,
            'The parking space was updated successfully',
            200
        );
    }

    /**
     * Remove the given parking space from database
     *
     * @param Request $request
     * @return mixed
     */
    public function delete( Request $request )
    {
        $this->parkingSpace->delete( $request );

        return $this->makeResponse(
            true,
            'The parking space was deleted successfully',
            200
        );
    }


}
